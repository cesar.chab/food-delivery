<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DriverRestaurant extends Model 
{
    public $table = 'driver_restaurants';
    protected $primaryKey = 'user_id';
    public $timestamps = false;

    public $fillable = [
        'user_id',
        'restaurant_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'integer',
        'restaurant_id' => 'integer',
    ];
    
}
