<?php

namespace App\Repositories;

use App\Models\DriverRestaurant as ModelsDriverRestaurant;
// use Spatie\Permission\Models\UserRestaurant;
use InfyOm\Generator\Common\BaseRepository;

class DriverRestaurantRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'restaurant_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ModelsDriverRestaurant::class;
    }
}
